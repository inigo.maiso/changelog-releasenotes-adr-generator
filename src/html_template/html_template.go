package html_template

import (
	"fmt"
	"github.com/yosssi/gohtml"
	"time"
)

type AllowedHtmlTag string

const (
	HEADER      AllowedHtmlTag = "header"
	H1          AllowedHtmlTag = "h1"
	H2          AllowedHtmlTag = "h2"
	H3          AllowedHtmlTag = "h3"
	H4          AllowedHtmlTag = "h4"
	DIV         AllowedHtmlTag = "div"
	P           AllowedHtmlTag = "p"
	BULLET_LIST AllowedHtmlTag = "ul"
	LIST_ENTRY  AllowedHtmlTag = "li"
	SECTION     AllowedHtmlTag = "section"
	ARTICLE     AllowedHtmlTag = "article"
)

type HtmlElement struct {
	Tag     AllowedHtmlTag
	Content string
}

func ToStringHtmlElements(htmlElements []HtmlElement) string {
	var content string
	for i := 0; i < len(htmlElements); i++ {
		content += ToStringHtmlElement(htmlElements[i])
	}
	return content
}

func ToStringHtmlElement(htmlElement HtmlElement) string {
	return fmt.Sprintf("<%s>%s</%s>", htmlElement.Tag, htmlElement.Content, htmlElement.Tag)
}

type MyHtmlTemplate struct {
	Title    string
	Elements []HtmlElement
}

func WrapContentWithTag(tag AllowedHtmlTag, content string) HtmlElement {
	return HtmlElement{
		Tag:     tag,
		Content: content,
	}
}

func Render(html MyHtmlTemplate) string {
	var body = ""
	for i := 0; i < len(html.Elements); i++ {
		body += ToStringHtmlElement(html.Elements[i])
	}
	return gohtml.Format(createHtmlDocument(html.Title, body))
}

func createHtmlDocument(title string, body string) string {
	return fmt.Sprintf(`
	<!DOCTYPE html>
	<html>
	<head>
	<title>%s</title>
	<meta charset="UTF-8">
	</head>
	<body>
	%s
	<footer>Platform-Name %v</footer>
	</body>
	</html>`, title, body, time.Now().Year())
}
