package html_template

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRender(t *testing.T) {
	var elements = []HtmlElement{
		HtmlElement{
			Tag:     DIV,
			Content: "Example div content1",
		},
		HtmlElement{
			Tag:     DIV,
			Content: "Example div content2",
		},
	}

	var template = MyHtmlTemplate{
		Title:    "ExampleTitle",
		Elements: elements,
	}
	const expectedTemplate = "<!DOCTYPE html>\n<html>\n  <head>\n    <title>\n      ExampleTitle\n    </title>\n    <meta charset=\"UTF-8\">\n  </head>\n  <body>\n    <div>\n      Example div content1\n    </div>\n    <div>\n      Example div content2\n    </div>\n    <footer>\n      Platform-Name 2021\n    </footer>\n  </body>\n</html>"

	var content = Render(template)
	assert.Equal(t, expectedTemplate, content)
}
