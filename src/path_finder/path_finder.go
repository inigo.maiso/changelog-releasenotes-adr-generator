package path_finder

import (
	"log"
	"os"
	"path/filepath"
	"showmeyourcode/cra-generator/src/constant"
	"time"
)

func GetPathLastElement(path string) (rootPath string, lastElement string) {
	return filepath.Split(path)
}

func CombinePath(path string, element string) string {
	return filepath.Join(path, element)
}

func addSuffixFromSourceDirectory(directoryName string) string {
	return CombinePath("source", directoryName)
}

func GetReleaseNoteFolderPath(workingDirectoryPath string) string {
	return CombinePath(workingDirectoryPath, addSuffixFromSourceDirectory(constant.ReleaseNoteDirectory))
}

func GetChangelogFolderPath(workingDirectoryPath string) string {
	return CombinePath(workingDirectoryPath, addSuffixFromSourceDirectory(constant.ChangelogDirectory))
}

func GetComponentsDescriptionFolderPath(workingDirectory string) string {
	return CombinePath(workingDirectory, addSuffixFromSourceDirectory(constant.ComponentsDescriptionDirectory))
}

func GetReleaseNoteFolderOutputPath(version string, suffix string) string {
	if suffix == "" {
		suffix = ".html"
	}
	var fileName = constant.ReleaseNoteFileName + version + "-" + time.Now().Format("2006-01-02") + suffix
	return CombinePath(
		CombinePath(CombinePath(GetWorkingDirectoryPath(), constant.OutputDirectory), constant.ReleaseNoteDirectory),
		fileName)
}

func GetChangelogFolderOutputPath(componentName string, version string, suffix string) string {
	if suffix == "" {
		suffix = ".html"
	}
	var fileName = constant.ChangelogFileName + componentName + " " + version + " - " + time.Now().Format("2006-01-02") + suffix
	return CombinePath(
		CombinePath(CombinePath(GetWorkingDirectoryPath(), constant.OutputDirectory), constant.ChangelogDirectory),
		fileName)
}

func GetWorkingDirectoryPath() string {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	return dir
}
