package path_finder

import (
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func TestGetReleaseNoteFolderOutputPath(t *testing.T) {
	var releaseNoteFolderOutputPath = GetReleaseNoteFolderOutputPath("X.Y.Z", "")
	assert.True(t, strings.Contains(releaseNoteFolderOutputPath, "ReleaseNote-X.Y.Z-"))
}

func TestGetChangelogFolderOutputPath(t *testing.T) {
	var changelogFolderOutputPath = GetChangelogFolderOutputPath("component1", "X.Y.Z", "")
	assert.True(t, strings.Contains(changelogFolderOutputPath, "Changelog-component1 X.Y.Z"))
}

func TestGetWorkingDirectoryPath(t *testing.T) {
	var workingDirectoryPath = GetWorkingDirectoryPath()
	assert.True(t, strings.Contains(workingDirectoryPath, "path_finder"))
}
