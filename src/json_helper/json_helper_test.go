package json_helper

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseJsonObject(t *testing.T) {
	const path = "./test-json/test-object.json"
	var _, object = ParseJsonObject(path)
	assert.NotNil(t, object)
}

func TestParseJsonArray(t *testing.T) {
	const path = "./test-json/test-array.json"
	var _, array = ParseJsonArray(path)
	assert.NotNil(t, array)
}
