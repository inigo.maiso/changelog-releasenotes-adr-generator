package json_helper

import (
	"encoding/json"
	"showmeyourcode/cra-generator/src/custom_io_tool"
	"showmeyourcode/cra-generator/src/path_finder"
	"strings"
)

func ParseJsonObject(path string) (string, map[string]string) {
	var _, lastElementName = path_finder.GetPathLastElement(path)
	var dat map[string]string
	json.Unmarshal([]byte(custom_io_tool.LoadFileContent(path)), &dat)
	var pathElements = strings.Split(lastElementName, ".")
	return pathElements[len(pathElements)-1], dat
}

func ParseJsonArray(path string) (string, []map[string]string) {
	var _, lastElementName = path_finder.GetPathLastElement(path)
	var dat []map[string]string
	json.Unmarshal([]byte(custom_io_tool.LoadFileContent(path)), &dat)
	var pathElements = strings.Split(lastElementName, ".")
	return pathElements[len(pathElements)-2], dat
}
