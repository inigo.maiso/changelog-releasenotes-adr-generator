package command

type Command interface {
	Execute(sourcePath string)
	PrintInfo()
}
