# Changelog ReleaseNotes ADR Generator

| Branch        | Pipeline          | Code coverage  |
| ------------- |:-----------------:| --------------:|
| master       | [![pipeline status](https://gitlab.com/ShowMeYourCodeYouTube/changelog-releasenotes-adr-generator/badges/master/pipeline.svg)](https://gitlab.com/ShowMeYourCodeYouTube/changelog-releasenotes-adr-generator/-/commits/master)  | [![coverage report](https://gitlab.com/ShowMeYourCodeYouTube/changelog-releasenotes-adr-generator/badges/master/coverage.svg)](https://gitlab.com/ShowMeYourCodeYouTube/changelog-releasenotes-adr-generator/-/commits/master) |
| develop      | [![pipeline status](https://gitlab.com/ShowMeYourCodeYouTube/changelog-releasenotes-adr-generator/badges/develop/pipeline.svg)](https://gitlab.com/ShowMeYourCodeYouTube/changelog-releasenotes-adr-generator/-/commits/develop)      |   [![coverage report](https://gitlab.com/ShowMeYourCodeYouTube/changelog-releasenotes-adr-generator/badges/develop/coverage.svg)](https://gitlab.com/ShowMeYourCodeYouTube/changelog-releasenotes-adr-generator/-/commits/develop) |

A simple tool which can be used to generate documents e.g. changelog, release notes or ADR (Architecture Decision Record). It was written in order to get familiar with Go language.

Test code coverage: https://showmeyourcodeyoutube.gitlab.io/changelog-releasenotes-adr-generator/coverage-report.html#file0

Prerequisites:
- Golang 1.16.x

![Demo](./demo.gif)

## Project Setup
The project uses modules, it was set up using commands:
- `go mod init showmeyourcode/cra-generator`
- `go mod tidy`

The preferred IDE is GoLand or IntelliJ with Go plugin because you can easily configure environments and install necessary modules.  

## Project structure
- **examples**
- **output**
- **source** | the folder's purpose is to store data like json file contain information (used to generate files by script) or .md files
   - adr
   - changelogs
      - x.x.x | version
         - <component_name> | name of the microservice/component which is part of the platform
            - fixed.json
            - removed.json
         - details.json | file contains basic information about release based
   - components-description
   - release-notes
      - x.x.x | version
         - fixed.json
         - new.json
         - removed.json
         - details.json | file contains basic information about release based
- **src** | program's scripts

## Release note
Release notes are a set of documents delivered to customers with the intent to provide a verbose description of the release of a new version of a product or service.
These artifacts are generally created by a marketing team or product owner and contain feature summaries, bug fixes, use cases, and other support material.
The release notes are used as a quick guide to what changed outside of the user documentation.  
*Remember*
1. Do not include minor changes (such changes might be included in changelog).
1. Write release notes entries in the past tense.
1. Omit phrases first person like "I have fixed", or references to the pull request like "this pull request fixes".

*Examples*
- https://www.appcues.com/blog/release-notes-examples

## Changelog
A changelog is a log or record of all notable changes made to a project - new features, enhancements, bugs, and other changes in reverse chronological order.
Changelogs usually link to specific issues or feature requests within a change management system and also may include links to the developer who supplied the change.
The template you can find here: `examples/changelogs/template.md`.

## Architectural decision record (ADR)
An architectural decision record (ADR) is a document that captures an important architectural decision made along with its context and consequences.  
Reference: <https://github.com/joelparkerhenderson/architecture_decision_record>

An example template you can find here: `examples/adr/template.md`.
